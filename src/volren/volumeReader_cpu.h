template <class T, class A, class D, class O>
void createBricks_h(T* vol, cudaExtent& volumeSize, std::vector<VolumeBlock_hd<A, O>>& blocks) //CPU Implementation for now
{
	//std::vector<VolumeBlock_hd<A, O>> blocks; // filter double Blocks/ Bricks
	unsigned int unique_blocks = 0;
	{
		// create blocks
		std::vector<VolumeBlock_hd<A, O>>* hash_blocks = new std::vector<VolumeBlock_hd<A, O>>[65536];
		for (unsigned int z0 = 0; z0 < volumeSize.depth; z0 += 4)
		{
			for (unsigned int y0 = 0; y0 < volumeSize.height; y0 += 4)
			{
				for (unsigned int x0 = 0; x0 < volumeSize.width; x0 += 4)
				{
					VolumeBlock_hd<A, O> tmp;
					for (unsigned int z1 = 0; z1 < 4; z1++)
					{
						unsigned int z = std::min((unsigned int)(volumeSize.depth - 1), z0 + z1);
						for (unsigned int y1 = 0; y1 < 4; y1++)
						{
							unsigned int y = std::min((unsigned int)(volumeSize.height - 1), y0 + y1);
							for (unsigned int x1 = 0; x1 < 4; x1++)
							{
								unsigned int x = std::min((unsigned int)(volumeSize.width - 1), x0 + x1);
								//tmp.value[x1 + y1 * 4 + z1 * 16] = convert(vol[x + (y + z * volumeSize.height) * volumeSize.width]);
								tmp.value[x1 + y1 * 4 + z1 * 16] = vol[x + (y + z * volumeSize.height) * volumeSize.width];
							}
						}
					}
					tmp.calcHash();
					tmp.count = 1;
					tmp.last = 0;
					bool unique = true;
					for (unsigned int i = 0; (unique) && (i < hash_blocks[tmp.hash].size()); i++)
					{
						if (tmp == hash_blocks[tmp.hash][i])
						{
							unique = false;
							hash_blocks[tmp.hash][i].count++;
						}
					}
					if (unique)
					{
						hash_blocks[tmp.hash].push_back(tmp);
						unique_blocks++;
					}
				}
			}
		}
		blocks.resize(unique_blocks);
		unsigned int i = 0;

		for (unsigned int list = 0; list < 65536; list++)
		{
			for (unsigned int idx = 0; idx < hash_blocks[list].size(); idx++)
			{
				blocks[i++] = hash_blocks[list][idx];
			}
		}
		delete[] hash_blocks;
	}

	return;
}

template <class T, class A, class D, class O>
void bruteForceInit_h(std::vector<CenterBlock_hd<A, D, O>>& selected_blocks, std::vector<VolumeBlock_hd<A, O>>& blocks, unsigned int k)
{

	std::cout << "brute force init on host (" << print_OMP_cores() << ") : " << std::endl;
	unsigned int unique_blocks = (unsigned int) blocks.size();
	//std::vector<CenterBlock_hd<A, D, O>> selected_blocks(k);
	A global_min = A(0x7fffffffffffffffll);
	unsigned int min_idx = 0;

#pragma omp parallel for
	for (int i = 0; i < (int)unique_blocks; i++)
	{
		A p;
		p = blocks[i].dist();// * blocks[i].count;
		if (p <= global_min)
		{
#pragma omp critical
			{
				if ((p < global_min) || ((p == global_min) && (i < (int)min_idx)))
				{
					global_min = p;
					min_idx = i;
				}
			}
		}
	}
	selected_blocks[0] = blocks[min_idx];
	int next_idx = 0;
	A global_max = A(0);
	// update distances
	{
		CenterBlock_hd<A, D, O>& center = selected_blocks[0];
#pragma omp parallel for
		for (int i = 0; i < (int)unique_blocks; i++)
		{
			VolumeBlock_hd<A, O>& blk = blocks[i];
			A p = blk.lastDist = center.dist(blk);
			blk.last = 0;
			if (p >= global_max)
			{
#pragma omp critical
				{
					if ((p > global_max) || ((p == global_max) && (i < (int)next_idx)))
					{
						global_max = p;
						next_idx = i;
					}
				}
			}
		}
	}
	unsigned int done = 1;
	while (done < k)
	{
		selected_blocks[done] = blocks[next_idx];
		// update distances
		global_max = A(0);
		next_idx = 0;
		CenterBlock_hd<A, D, O>& center = selected_blocks[done];
#pragma omp parallel for
		for (int i = 0; i < (int)unique_blocks; i++)
		{
			VolumeBlock_hd<A, O>& blk = blocks[i];
			if (center.dist(blk, blk.lastDist, false))
			{
				blk.last = done;
			}
			A p = blk.lastDist;
			if ((done + 1 < k) && (p >= global_max))
			{
#pragma omp critical
				{
					if ((p > global_max) || ((p == global_max) && (i < (int)next_idx)))
					{
						global_max = p;
						next_idx = i;
					}
				}
			}
		}
		done++;
		if ((done * 80 / k) > ((done - 1) * 80 / k)) std::cout << ".";
	}
	return;
}

template <class T, class A, class D, class O>
void prioQueueInit_h(std::vector<CenterBlock_hd<A, D, O>>& selected_blocks, std::vector<VolumeBlock_hd<A, O>>& blocks, unsigned int k)
{
	std::cout << "prio queue init on host(" << print_OMP_cores() << "): " << std::endl;

	unsigned int unique_blocks = (unsigned int) blocks.size();
	//std::vector<CenterBlock_hd<A, D, O>> selected_blocks(k);
	std::vector<IndexedPriority_hd<A>> pri_container(unique_blocks);
	std::vector<unsigned int> selection(unique_blocks);
	A global_min = A(0x7fffffffffffffffll);
	unsigned int min_idx = 0;

#pragma omp parallel for
	for (int i = 0; i < (int)unique_blocks; i++)
	{
		A p;
		p = blocks[i].dist();// * blocks[i].count;
		pri_container[i].count = 0;
		pri_container[i].idx = i;
		pri_container[i].p = p;
		if (p <= global_min)
		{
#pragma omp critical
			{
				if ((p < global_min) || ((p == global_min) && (i < (int)min_idx)))
				{
					global_min = p;
					min_idx = i;
				}
			}
		}
	}

	std::swap(pri_container[min_idx], pri_container[unique_blocks - 1]);
	pri_container.pop_back();
	selection[0] = min_idx;

	std::priority_queue<IndexedPriority_hd<A>, std::vector<IndexedPriority_hd<A>>, LessThan_hd<IndexedPriority_hd<A>>> pri_queue(LessThan_hd<IndexedPriority_hd<A>>(), pri_container);

	unsigned int done = 1;
	unsigned int last_done = 0;
	while (done < k)
	{
		IndexedPriority_hd<A> tmp = pri_queue.top();
		pri_queue.pop();

		A p = tmp.p;// / blocks[tmp.idx].count;
		int total = done - tmp.count;
#pragma omp parallel for
		for (int idx = 0; idx < total; idx++)
		{
			A pp;
			if (blocks[tmp.idx].dist(blocks[selection[tmp.count + idx]], p, pp))
			{
				if (pp < p)
				{
#pragma omp critical
					{
						if (pp < p)
						{
							p = pp;
						}
					}
				}
			}
		}
		tmp.p = p;// * blocks[tmp.idx].count;
		tmp.count = done;

		if (tmp.p >= pri_queue.top().p)
		{
			// still the furthest
			selection[done++] = tmp.idx;
			if ((done * 80 / k) > (last_done * 80 / k)) std::cout << ".";
			last_done = done;
		}
		else
		{
			// push back onto queue
			pri_queue.push(tmp);
		}
	}
	for (unsigned int i = 0; i < k; i++)
	{
		selected_blocks[i] = blocks[selection[i]];
	}
	return;
}

template <class T, class A, class D, class O>
void doRelaxation_h(T* vol, std::vector<VolumeBlock_hd<A, O>>& blocks, std::vector<CenterBlock_hd<A, D, O>>& selected_blocks, cudaExtent& volumeSize, unsigned int k, bool bruteForce)
{

	std::cout << "doing relaxation on host(" << print_OMP_cores() << "): " << std::endl;


	const unsigned int unique_blocks = (unsigned int) blocks.size();
	unsigned int* changed_blocks = new unsigned int[k];
	for (unsigned int i = 0; i < k; i++) changed_blocks[i] = i;
	unsigned int changed_blocks_size = k;

	unsigned int changed = changed_blocks_size;

	int run = 0;

	//double psnr_tmp = 0.0;

	while ((changed > 0) && (run < 1000)) // algorithm 4, already cws attached to cluster
	{
		changed = 0;
		unsigned int switched = 0;
		double psnr_dlt = 0.0;

		//maybe kernel 1
#pragma omp parallel for reduction(+: psnr_dlt, switched)
		for (int idx = 0; idx < (int)unique_blocks; idx++)
		{
			VolumeBlock_hd<A, O>& blk = blocks[idx];
			CenterBlock_hd<A, D, O>& center = selected_blocks[blk.last];
			unsigned int c = blk.last;
			A min_dist = A(0);

			if ((run == 0) && (bruteForce))
			{
				min_dist = blk.lastDist;
			}
			else
			{
				if (center.changedCenter)
				{
					min_dist = center.dist(blk);
				}
				else
					min_dist = blk.lastDist;

				if ((center.changedCenter) && (min_dist > blk.lastDist))
				{
					// increased distance so need to check all
					for (unsigned int i = 0; i < k; i++)
					{
						if ((min_dist == 0) && (i > c)) break;
						if (i != blk.last)
						{
							CenterBlock_hd<A, D, O>& comp = selected_blocks[i];
							if (comp.dist(blk, min_dist, (i < c)))
							{
								c = i;
							}
						}
					}
				}
				else
				{
					// distance decreased or unchanged, only check modified blocks
					for (unsigned int ii = 0; ii < changed_blocks_size; ii++)
					{
						unsigned int i = changed_blocks[ii];
						if ((min_dist == 0) && (i > c)) break;
						if (i != blk.last)
						{
							CenterBlock_hd<A, D, O>& comp = selected_blocks[i];
							if (comp.dist(blk, min_dist, (i < c)))
							{
								c = i;
							}
						}
					}
				}
			}
			CenterBlock_hd<A, D, O>& updated = selected_blocks[c];

			if ((blk.last != c) || (run == 0))
			{
				switched++;
				if (run > 0)
				{
					center.sub(blk);
				}
				updated.add(blk);
				center.changedMember = true;
				updated.changedMember = true;
				blk.last = c;
			}
			{
				double add = 0.0;
				//if (run > 0) add -= (double)blk.lastDist * (double)blk.count;
				add += (double)min_dist * (double)blk.count;
				psnr_dlt += add;
			}
			blk.lastDist = min_dist;
		}
		//psnr_tmp = psnr_dlt;
		changed_blocks_size = 0;

		//maybe kernel 2 ??
#pragma omp parallel for
		for (int i = 0; i < (int)k; i++)
		{
			selected_blocks[i].update();
		}

		for (int i = 0; i < (int)k; i++)
		{
			if (selected_blocks[i].changedCenter)
			{
				changed_blocks[changed_blocks_size++] = i;
			}
		}
		// calculate PSNR for comparing the quality of both data sets and images //see VMV 6.1
		/* {
			float psnr = (float)psnr_tmp;
			float a = quantizeMaximum_hd(vol);
			float b = 0.0f;
			psnr /= (float)(volumeSize.width * volumeSize.height * volumeSize.depth);
			psnr = (a - b) * (a - b) / psnr;
			psnr = 10.0f * logf(psnr) / logf(10.0f);
			std::cout << "  PSNR: " << psnr << "db" << std::endl;
		}*/
		changed = changed_blocks_size;
		run++;
		std::cout << " cpu iteration " << run << ": " << changed << " blocks changed (" << switched << " switched owner)." << std::endl;
	}
	delete[] changed_blocks;
}

template <class T, class A, class D, class O>
float recreateLookuptable_h(T* vol, std::vector<VolumeBlock_hd<A, O>>& blocks, std::vector<CenterBlock_hd<A, D, O>>& selected_blocks, cudaExtent& volumeSize)
{
	std::vector<VolumeBlock_hd<A, O>>* hash_blocks = new std::vector<VolumeBlock_hd<A, O>>[65536];
	for (unsigned int i = 0; i < blocks.size(); i++)
	{
		hash_blocks[blocks[i].hash].push_back(blocks[i]);
	}

	float psnr = 0.0f;
	for (unsigned int z0 = 0; z0 < volumeSize.depth; z0 += 4)
	{
		for (unsigned int y0 = 0; y0 < volumeSize.height; y0 += 4)
		{
			for (unsigned int x0 = 0; x0 < volumeSize.width; x0 += 4)
			{
				VolumeBlock_hd<A, O> tmp;
				for (unsigned int z1 = 0; z1 < 4; z1++)
				{
					unsigned int z = std::min((unsigned int)(volumeSize.depth - 1), z0 + z1);
					for (unsigned int y1 = 0; y1 < 4; y1++)
					{
						unsigned int y = std::min((unsigned int)(volumeSize.height - 1), y0 + y1);
						for (unsigned int x1 = 0; x1 < 4; x1++)
						{
							unsigned int x = std::min((unsigned int)(volumeSize.width - 1), x0 + x1);
							//tmp.value[x1 + y1 * 4 + z1 * 16] = convert(vol[x + (y + z * volumeSize.height) * volumeSize.width]);
							tmp.value[x1 + y1 * 4 + z1 * 16] = vol[x + (y + z * volumeSize.height) * volumeSize.width];
						}
					}
				}
				tmp.calcHash();
				unsigned int c = 0;
				for (unsigned int i = 0; i < hash_blocks[tmp.hash].size(); i++)
				{
					if (tmp == hash_blocks[tmp.hash][i]) c = hash_blocks[tmp.hash][i].last;
				}
				// strictly speaking this doesn't work since the data outside the volume might not match the data at the border.
				for (unsigned int z1 = 0; (z1 < 4) && (z0 + z1 < volumeSize.depth); z1++)
				{
					unsigned int z = z0 + z1;
					for (unsigned int y1 = 0; (y1 < 4) && (y0 + y1 < volumeSize.height); y1++)
					{
						unsigned int y = y0 + y1;
						for (unsigned int x1 = 0; (x1 < 4) && (x0 + x1 < volumeSize.width); x1++)
						{
							unsigned int x = x0 + x1;
							{
								int v = x1 + y1 * 4 + z1 * 16;
								psnr += calcPsnr_hd<CenterBlock_hd<A, D, O>, T>(vol[x + (y + z * volumeSize.height) * volumeSize.width], selected_blocks[c], v);
							}
						}
					}
				}
			}
		}
	}

	delete[] hash_blocks;
	return psnr;
}
