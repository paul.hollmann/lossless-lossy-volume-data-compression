// Copyright (c) 2016 Stefan Guthe / GCC / TU-Darmstadt. All rights reserved. 
// Use of this source code is governed by the BSD 3-Clause license that can be
// found in the LICENSE file.

#pragma once
#include "cuda_runtime.h"

typedef unsigned int  uint;
typedef unsigned char uchar;

typedef unsigned char VolumeType8;
typedef unsigned short VolumeType16;
typedef uchar4 VolumeType32;
typedef ushort4 VolumeType64;

template <class T, class A, class D, class O>
void quantizeVolume_full(T *vol, cudaExtent &volumeSize, int lossy, bool bruteForce, bool bruteForceOnGPU, bool relax_on_gpu);

