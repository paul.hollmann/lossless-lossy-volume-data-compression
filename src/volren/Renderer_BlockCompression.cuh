unsigned int div_up(unsigned int numerator, unsigned int denominator)
{
	unsigned int result = numerator / denominator;
	if (numerator % denominator) ++result;
	return result;
}

/*template <class T>
__host__ void RendererRBUC8x8<T>::callDevice1(T* h_volume, cudaExtent& volumeSize, bool& g_fast,
	unsigned int& double_ref, unsigned int& unique,
	size_t& c_offset,
	unsigned char* h_compressedVolume, size_t* h_compressedVolumeUInt,
	std::vector<std::vector<std::vector<T> > >& ref,
	std::vector<std::vector<std::vector<float> > >& vq_avg, std::vector<std::vector<unsigned int> >& vq_count,
	std::vector<std::vector<unsigned int> >& match_ref,
	std::vector<std::pair<unsigned int, unsigned int> >& vq_ref)
{
	// Speicher anlegen
	T* d_volume;
	unsigned char* d_compressedVolume;
	size_t* d_compressedVolumeUInt;

	size_t size = volumeSize.width * volumeSize.height * volumeSize.depth; // malloc size *= sizeof(VolumeType8)
	size_t cv_size = ((9 * sizeof(T) * (volumeSize.width * volumeSize.height * volumeSize.depth)) / 8);
	size_t cvui_size = cv_size >> 3;

	checkCudaErrors(cudaMalloc(&d_volume, sizeof(T) * size));
	checkCudaErrors(cudaMalloc(&d_compressedVolume, sizeof(unsigned char) * cv_size));
	checkCudaErrors(cudaMalloc(&d_compressedVolumeUInt, sizeof(size_t) * cvui_size));

	checkCudaErrors(cudaMemcpy(d_volume, h_volume, sizeof(T) * size, cudaMemcpyHostToDevice)); // ?Count
	checkCudaErrors(cudaMemcpy(d_compressedVolume, h_compressedVolume, sizeof(unsigned char) * cv_size, cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(d_compressedVolumeUInt, h_compressedVolumeUInt, sizeof(size_t) * cvui_size, cudaMemcpyHostToDevice));


	// Kernel berechnen
	// Gesamt Dimension: 
	unsigned int r_width = (unsigned int)((volumeSize.width + 3) >> 2);
	unsigned int r_height = (unsigned int)((volumeSize.height + 3) >> 2);
	unsigned int r_depth = (unsigned int)((volumeSize.depth + 3) >> 2);
	dim3 dimBlock(8, 8, 8);
	dim3 dimGrid((int)div_up(r_width, dimBlock.x), (int)div_up(r_height, dimBlock.y), (int)div_up(r_depth, dimBlock.z));
	// callDevice2<<<dimGrid, dimBlock>>>();

	// Ergebnisse von Device zurückholen
	checkCudaErrors(cudaMemcpy(h_volume, d_volume, sizeof(T) * size, cudaMemcpyDeviceToHost)); // ?Count
	checkCudaErrors(cudaMemcpy(h_compressedVolume, d_compressedVolume, sizeof(unsigned char) * cv_size, cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(h_compressedVolumeUInt, d_compressedVolumeUInt, sizeof(size_t) * cvui_size, cudaMemcpyDeviceToHost));


	checkCudaErrors(cudaFree(h_volume));
	checkCudaErrors(cudaFree(h_compressedVolume));
	checkCudaErrors(cudaFree(h_compressedVolumeUInt));
}

template <class T>
__global__ void callDevice2(T* d_volume, cudaExtent volumeSize, bool g_fast,
	unsigned int& double_ref, unsigned int& unique,
	size_t& c_offset,
	unsigned char* d_compressedVolume, size_t* d_compressedVolumeUInt,
	std::vector<std::vector<std::vector<T> > >& ref,
	std::vector<std::vector<std::vector<float> > >& vq_avg, std::vector<std::vector<unsigned int> >& vq_count,
	std::vector<std::vector<unsigned int> >& match_ref,
	std::vector<std::pair<unsigned int, unsigned int> >& vq_ref)
{
	unsigned int x = blockIdx.x * dimBlock.x + threadIdx.x;
	unsigned int y = blockIdx.y * dimBlock.y + threadIdx.y;
	unsigned int z = blockIdx.z * dimBlock.z + threadIdx.z;


	T raw_dat[64];
	std::vector<T> tmp;
	int key = 0;
	bool data_valid = false;
	if (!g_fast)
	{
		if ((z * 4 + 4 > volumeSize.depth) || (y * 4 + 4 > volumeSize.height) || (x * 4 + 4 > volumeSize.width))
		{
			unsigned int zl = std::min(4u, (unsigned int)(volumeSize.depth - (z << 2)));
			unsigned int yl = std::min(4u, (unsigned int)(volumeSize.height - (y << 2)));
			unsigned int xl = std::min(4u, (unsigned int)(volumeSize.width - (x << 2)));
			// we are at the border so do a brute-force search for a matching block to complete this one
			for (unsigned int za = 0; (za < volumeSize.depth >> 2) && (!data_valid); za++)
			{
				for (unsigned int ya = 0; (ya < volumeSize.height >> 2) && (!data_valid); ya++)
				{
					for (unsigned int xa = 0; (xa < volumeSize.depth >> 2) && (!data_valid); xa++)
					{
						data_valid = true;
						for (unsigned int z0 = 0; (z0 < zl) && data_valid; z0++)
						{
							unsigned int zr = (z << 2) + z0;
							unsigned int zc = (za << 2) + z0;
							for (unsigned int y0 = 0; (y0 < yl) && data_valid; y0++)
							{
								unsigned int yr = (y << 2) + y0;
								unsigned int yc = (ya << 2) + y0;
								size_t xr = (x << 2) + volumeSize.width * (yr + volumeSize.height * zr);
								size_t xc = (xa << 2) + volumeSize.width * (yc + volumeSize.height * zc);
								for (unsigned int x0 = 0; (x0 < xl) && data_valid; x0++)
								{
									T val = h_volume[xr++];
									T alt = h_volume[xc++];
									data_valid = (val == alt);
								}
							}
						}
						if (data_valid)
						{
							for (unsigned int z0 = 0; z0 < 4; z0++)
							{
								unsigned int zr = (za << 2) + z0;
								for (unsigned int y0 = 0; y0 < 4; y0++)
								{
									unsigned int yr = (ya << 2) + y0;
									for (unsigned int x0 = 0; x0 < 4; x0++)
									{
										unsigned int xr = (xa << 2) + x0;
										T val = h_volume[xr + volumeSize.width * (yr + volumeSize.height * zr)];
										raw_dat[x0 + ((y0 + (z0 << 2)) << 2)] = val;
										tmp.push_back(val);
										key = (key * 13) + getVal<T>(val);
									}
								}
							}
						}
					}
				}
			}
			// now we still need to check the other borders...
		}
	}
	// check if we already filled the data
	if (!data_valid)
	{
		for (unsigned int z0 = 0; z0 < 4; z0++)
		{
			unsigned int zr = std::min((unsigned int)(volumeSize.depth - 1), (z << 2) + z0);
			for (unsigned int y0 = 0; y0 < 4; y0++)
			{
				unsigned int yr = std::min((unsigned int)(volumeSize.height - 1), (y << 2) + y0);
				for (unsigned int x0 = 0; x0 < 4; x0++)
				{
					unsigned int xr = std::min((unsigned int)(volumeSize.width - 1), (x << 2) + x0);
					T val = h_volume[xr + volumeSize.width * (yr + volumeSize.height * zr)];
					raw_dat[x0 + ((y0 + (z0 << 2)) << 2)] = val;
					tmp.push_back(val);
					key = (key * 13) + getVal<T>(val);
				}
			}
		}
	}
	// 24 bit
	key &= 0xffffff;
	// check if we got the same data already
	int match_idx = -1;
	{
		for (unsigned int i = 0; ((match_idx == -1) && (i < ref[key].size())); i++)
		{
			bool match = true;
			bool all_zero = true;
			for (unsigned int j = 0; (match && (j < tmp.size())); j++)
			{
				match = (ref[key][i][j] == tmp[j]);
				all_zero &= (tmp[j] == 0);
			}
			if (match) match_idx = match_ref[key][i];
			if (match && all_zero) global_count[4]++;
		}
	}
	unsigned int size = 0;
	{
		if (match_idx == -1)
		{
			h_compressedVolumeUInt[x + r_width * (y + r_height * z)] = c_offset;
			size = compress(raw_dat, &(h_compressedVolume[c_offset]));
		}
		if (match_idx == -1)
		{
			ref[key].push_back(tmp);
			match_ref[key].push_back(cur_idx);
			c_offset += size;
			unique++;
		}
		else
		{
			h_compressedVolumeUInt[x + r_width * (y + r_height * z)] = h_compressedVolumeUInt[match_idx];
			double_ref++;
		}
	}
	cur_idx++;
}*/