// Copyright (c) 2016 Stefan Guthe / GCC / TU-Darmstadt. All rights reserved. 
// Use of this source code is governed by the BSD 3-Clause license that can be
// found in the LICENSE file.

#pragma once
#include "global_defines.h"

#include "Renderer.h"
#include "cuda.h"
#include <algorithm>

// class definitions

template <class T>
class RendererRBUC8x8 : public Renderer<T>
{
private:
	cudaArray *d_transferFuncArray;
	bool m_bLinearFilter;
	cudaExtent m_extent;
	cudaExtent m_realExtent;
	float3 m_scale;
	bool m_rgba;
	bool m_lighting;
	float m_maximum;

	bool volume64;
	// ugly but working
	void *offsetVolume;
	void *compressedVolume;

	cudaTextureObject_t m_transferTex;

	float *invViewMatrix;
	float *viewMatrix;
	size_t sizeofMatrix;
	unsigned int m_components;
public:
	RendererRBUC8x8()
	{
		d_transferFuncArray = 0;
		m_bLinearFilter = false;
		m_rgba = false;
		m_lighting = false;
		m_components = 0;
		volume64 = false;
		offsetVolume = NULL;
		compressedVolume = NULL;
	}
	virtual ~RendererRBUC8x8()
	{
		if (offsetVolume != NULL)
		{
			if (volume64)
			{
				((OffsetVolume<uint64>*)offsetVolume)->destroy();
				delete((OffsetVolume<uint64>*)offsetVolume);
			}
			else
			{
				((OffsetVolume<uint>*)offsetVolume)->destroy();
				delete((OffsetVolume<uint>*)offsetVolume);
			}
			offsetVolume = NULL;
		}
		if (compressedVolume != NULL)
		{
			// this does not have a pointer created with cudaMalloc
			//((CompressedVolume<T>*)compressedVolume)->destroy();
			delete((CompressedVolume<T>*)compressedVolume);
			compressedVolume = NULL;
		}
	};

	virtual void setTextureFilterMode(bool bLinearFilter) override ;
	virtual void initCuda(T *h_volume, cudaExtent volumeSize, cudaExtent originalSize, float3 scale, unsigned int components, int max_error) override;
	virtual void updateTF() override;
	virtual void freeCudaBuffers() override;
	virtual void render_kernel(dim3 gridSize, dim3 blockSize, dim3 warpDim, uint *d_output, uint imageW, uint imageH,
							   float density, float brightness, float transferOffset, float transferScale, float tstep, bool white) override ;
	virtual void copyInvViewMatrix(float *invViewMatrix, size_t sizeofMatrix) override ;
	virtual void copyViewMatrix(float *viewMatrix, size_t sizeofMatrix) override;

	virtual void enableRGBA(bool rgba) override { m_rgba = rgba; }

	virtual void enableLighting(bool lighting) override { m_lighting = lighting; }

	virtual void getLaunchParameter(dim3& warpDim, uint imageW, uint imageH, float tstep) override ;

	virtual void setScale(float3 scale) override { m_scale = scale; }

private:
	unsigned int compress(T *raw, unsigned char *comp);
	unsigned int setMinMax(unsigned char *comp, int min, int max);

private:
	template <uint wsx, uint wsy, uint wsz>
	void render_internal_kernel(dim3 gridSize, dim3 blockSize, uint *d_output, uint imageW, uint imageH,
		float density, float brightness, float transferOffset, float transferScale, float tstep, bool white);
};

template <typename T>
class PrivateBitStream
{
private:
	T pos;
	uint off;
	T* data;
	T temp;
	uint Tbits;
public:
	PrivateBitStream(T* data) : data(data) { pos = 0; off = 0; temp = 0; Tbits = sizeof(T) << 3; }
	~PrivateBitStream() {}

	void write(T value, uint bits)
	{
		T val = value & ((T(1) << bits) - T(1));
		temp |= val << off;
		off += bits;
		if (off >= Tbits)
		{
			off -= Tbits;
			data[pos++] = temp;
			temp = val >> (bits - off);
		}
	}

	void flush()
	{
		if (off > 0)
		{
			data[pos++] = temp;
			off = 0;
			temp = 0;
		}
	}

	T size()
	{
		return pos * sizeof(T);
	}
};

// helper functions for 4 types

template <class T> unsigned short getVal(T& a);

template<> unsigned short getVal<unsigned char>(unsigned char& a) { return (unsigned short)a; }
template<> unsigned short getVal<unsigned short>(unsigned short& a) { return a; }
template<> unsigned short getVal<uchar4>(uchar4& a)
{
	unsigned short r;
	r = (unsigned short)a.x;
	r *= 17;
	r += (unsigned short)a.y;
	r *= 17;
	r += (unsigned short)a.z;
	r *= 17;
	r += (unsigned short)a.w;
	return r;
}
template<> unsigned short getVal<ushort4>(ushort4& a)
{
	unsigned short r;
	r = a.x;
	r *= 17;
	r += a.y;
	r *= 17;
	r += a.z;
	r *= 17;
	r += a.w;
	return r;
}

template <typename T> inline float getMax(T& v);
template <> inline float getMax(unsigned char& v) { return (float)v; }
template <> inline float getMax(unsigned short& v) { return (float)v; }
template <> inline float getMax(uchar4& v) { return (float)std::max(std::max(v.x, v.y), std::max(v.z, v.w)); }
template <> inline float getMax(ushort4& v) { return (float)std::max(std::max(v.x, v.y), std::max(v.z, v.w)); }

template <> unsigned int RendererRBUC8x8<unsigned char>::setMinMax(unsigned char* comp, int min, int max)
{
	comp[0] = min;
	comp[1] = max;
	return 2;
}

template <> unsigned int RendererRBUC8x8<unsigned short>::setMinMax(unsigned char* comp, int min, int max)
{
	comp[0] = (unsigned char)min;
	comp[1] = (unsigned char)(min >> 8);
	comp[2] = (unsigned char)max;
	comp[3] = (unsigned char)(max >> 8);
	return 4;
}

template <> unsigned int RendererRBUC8x8<uchar4>::setMinMax(unsigned char* comp, int min, int max)
{
	comp[0] = min;
	comp[1] = max;
	return 2;
}

template <> unsigned int RendererRBUC8x8<ushort4>::setMinMax(unsigned char* comp, int min, int max)
{
	comp[0] = (unsigned char)min;
	comp[1] = (unsigned char)(min >> 8);
	comp[2] = (unsigned char)max;
	comp[3] = (unsigned char)(max >> 8);
	return 4;
}

template <class T> T getAvg(T& a, T& b);

template <> unsigned char getAvg(unsigned char& a, unsigned char& b) { return (unsigned char)(((int)a + (int)b + 1) >> 1); }
template <> unsigned short getAvg(unsigned short& a, unsigned short& b) { return (unsigned short)(((int)a + (int)b + 1) >> 1); }
template <> uchar4 getAvg(uchar4& a, uchar4& b) { return make_uchar4(getAvg(a.x, b.x), getAvg(a.y, b.y), getAvg(a.z, b.z), getAvg(a.w, b.w)); }
template <> ushort4 getAvg(ushort4& a, ushort4& b) { return make_ushort4(getAvg(a.x, b.x), getAvg(a.y, b.y), getAvg(a.z, b.z), getAvg(a.w, b.w)); }

template <class T> T encode(T& pred, T& min, T& max, T& raw);

template<> unsigned char encode(unsigned char& pred, unsigned char& min, unsigned char& max, unsigned char& raw) { return encodeInternal<unsigned char>(pred, min, max, raw); }
template<> unsigned short encode(unsigned short& pred, unsigned short& min, unsigned short& max, unsigned short& raw) { return encodeInternal<unsigned short>(pred, min, max, raw); }
template<> uchar4 encode(uchar4& pred, uchar4& min, uchar4& max, uchar4& raw)
{
	return make_uchar4(
		encodeInternal<unsigned char>(pred.x, min.x, max.x, raw.x),
		encodeInternal<unsigned char>(pred.y, min.y, max.y, raw.y),
		encodeInternal<unsigned char>(pred.z, min.z, max.z, raw.z),
		encodeInternal<unsigned char>(pred.w, min.w, max.w, raw.w));
}
template<> ushort4 encode(ushort4& pred, ushort4& min, ushort4& max, ushort4& raw)
{
	return make_ushort4(
		encodeInternal<unsigned short>(pred.x, min.x, max.x, raw.x),
		encodeInternal<unsigned short>(pred.y, min.y, max.y, raw.y),
		encodeInternal<unsigned short>(pred.z, min.z, max.z, raw.z),
		encodeInternal<unsigned short>(pred.w, min.w, max.w, raw.w));
}

unsigned char predict(unsigned char* raw, unsigned char avg, unsigned char min, unsigned char max, unsigned int x, unsigned int y, unsigned int z, unsigned int idx)
{
	int pred = 0;
	if ((x == 0) && (y == 0) && (z == 0)) pred = avg;
	if (x > 0) pred += raw[idx - 1];
	if (y > 0) pred += raw[idx - 4];
	if ((x > 0) && (y > 0)) pred -= raw[idx - 5];
	if (z > 0) pred += raw[idx - 16];
	if ((x > 0) && (z > 0)) pred -= raw[idx - 17];
	if ((y > 0) && (z > 0)) pred -= raw[idx - 20];
	if ((x > 0) && (y > 0) && (z > 0)) pred += raw[idx - 21];
	if (pred < min) return min;
	if (pred > max) return max;
	return (unsigned char)pred;
}
unsigned short predict(unsigned short* raw, unsigned short avg, unsigned short min, unsigned short max, unsigned int x, unsigned int y, unsigned int z, unsigned int idx)
{
	int pred = 0;
	if ((x == 0) && (y == 0) && (z == 0)) pred = avg;
	if (x > 0) pred += raw[idx - 1];
	if (y > 0) pred += raw[idx - 4];
	if ((x > 0) && (y > 0)) pred -= raw[idx - 5];
	if (z > 0) pred += raw[idx - 16];
	if ((x > 0) && (z > 0)) pred -= raw[idx - 17];
	if ((y > 0) && (z > 0)) pred -= raw[idx - 20];
	if ((x > 0) && (y > 0) && (z > 0)) pred += raw[idx - 21];
	if (pred < min) return min;
	if (pred > max) return max;
	return (unsigned short)pred;
}
uchar4 predict(uchar4* raw, uchar4 avg, uchar4 min, uchar4 max, unsigned int x, unsigned int y, unsigned int z, unsigned int idx)
{
	int4 pred = make_int4(0, 0, 0, 0);
	if ((x == 0) && (y == 0) && (z == 0)) pred = make_int4(avg.x, avg.y, avg.z, avg.w);
	if (x > 0) pred += raw[idx - 1];
	if (y > 0) pred += raw[idx - 4];
	if ((x > 0) && (y > 0)) pred -= raw[idx - 5];
	if (z > 0) pred += raw[idx - 16];
	if ((x > 0) && (z > 0)) pred -= raw[idx - 17];
	if ((y > 0) && (z > 0)) pred -= raw[idx - 20];
	if ((x > 0) && (y > 0) && (z > 0)) pred += raw[idx - 21];
	if (pred.x < min.x) pred.x = min.x;
	if (pred.y < min.y) pred.y = min.y;
	if (pred.z < min.z) pred.z = min.z;
	if (pred.w < min.w) pred.w = min.w;
	if (pred.x > max.x) pred.x = max.x;
	if (pred.y > max.y) pred.y = max.y;
	if (pred.z > max.z) pred.z = max.z;
	if (pred.w > max.w) pred.w = max.w;
	return make_uchar4(pred.x, pred.y, pred.z, pred.w);
}
ushort4 predict(ushort4* raw, ushort4 avg, ushort4 min, ushort4 max, unsigned int x, unsigned int y, unsigned int z, unsigned int idx)
{
	int4 pred = make_int4(0, 0, 0, 0);
	if ((x == 0) && (y == 0) && (z == 0)) pred = make_int4(avg.x, avg.y, avg.z, avg.w);
	if (x > 0) pred += raw[idx - 1];
	if (y > 0) pred += raw[idx - 4];
	if ((x > 0) && (y > 0)) pred -= raw[idx - 5];
	if (z > 0) pred += raw[idx - 16];
	if ((x > 0) && (z > 0)) pred -= raw[idx - 17];
	if ((y > 0) && (z > 0)) pred -= raw[idx - 20];
	if ((x > 0) && (y > 0) && (z > 0)) pred += raw[idx - 21];
	if (pred.x < min.x) pred.x = min.x;
	if (pred.y < min.y) pred.y = min.y;
	if (pred.z < min.z) pred.z = min.z;
	if (pred.w < min.w) pred.w = min.w;
	if (pred.x > max.x) pred.x = max.x;
	if (pred.y > max.y) pred.y = max.y;
	if (pred.z > max.z) pred.z = max.z;
	if (pred.w > max.w) pred.w = max.w;
	return make_ushort4(pred.x, pred.y, pred.z, pred.w);
}

unsigned char getMin(unsigned char a, unsigned char b) { return std::min(a, b); }
unsigned short getMin(unsigned short a, unsigned short b) { return std::min(a, b); }
uchar4 getMin(uchar4 a, uchar4 b) { return make_uchar4(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z), std::min(a.w, b.w)); }
ushort4 getMin(ushort4 a, ushort4 b) { return make_ushort4(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z), std::min(a.w, b.w)); }
unsigned char getMax(unsigned char a, unsigned char b) { return std::max(a, b); }
unsigned short getMax(unsigned short a, unsigned short b) { return std::max(a, b); }
uchar4 getMax(uchar4 a, uchar4 b) { return make_uchar4(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z), std::max(a.w, b.w)); }
ushort4 getMax(ushort4 a, ushort4 b) { return make_ushort4(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z), std::max(a.w, b.w)); }

// single functions

unsigned char decodeDeltaTest(unsigned char delta, unsigned char pred, unsigned char min, unsigned char max)
{
	unsigned char max_pos = max - pred;
	unsigned char max_neg = pred - min;
	// this is a corner case where the whole range is positive only
	if (max_neg == 0) return pred + delta;

	unsigned char balanced_max = __min(max_neg - 1, max_pos);
	if ((delta >> 1) > balanced_max)
	{
		if (max_pos >= max_neg)
		{
			return pred + delta - balanced_max - 1;
		}
		else
		{
			return pred - delta + balanced_max;
		}
	}
	if ((delta & 1) == 1)
		return pred - (delta >> 1) - 1;
	else
		return pred + (delta >> 1);
}

void transform(double& v0, double& v1)
{
	double sum = v0 + v1;
	double dif = v1 - v0;
	v0 = sqrt(0.5) * sum;
	v1 = sqrt(0.5) * dif;
}

void invTransform(double& v0, double& v1)
{
	double sum = sqrt(2.0) * v0;
	double dif = sqrt(2.0) * v1;
	v0 = 0.5 * (sum - dif);
	v1 = 0.5 * (sum + dif);
}

template <class T> T encodeInternal(T& pred, T& min, T& max, T& raw)
{
	int max_pos = (int)max - pred;
	int max_neg = (int)min - pred;
	int dlt = (int)raw - pred;
	// -max_neg = max_pos + 1 is the balanced case
	int m = (dlt < 0) ? -1 : 0;
	int balanced_max = std::min(max_neg ^ -1, max_pos);
	// balanced_max can be -1 if max_neg is 0
	if ((dlt ^ m) > balanced_max)
	{
		// off balance
		return (dlt ^ m) + (balanced_max + 1);
	}
	else
	{
		// balanced
		return (dlt << 1) ^ m;
	}
}

void BoxSplit(float3 a, float3& d, float3& e)
{
	if (a.x < 0.0f) d.x -= a.x;
	else e.x += a.x;
	if (a.y < 0.0f) d.y -= a.y;
	else e.y += a.y;
	if (a.z < 0.0f) d.z -= a.z;
	else e.z += a.z;
}

float BoxVol(float3 a, float3 b, float3 c)
{
	float3 d, e;
	d = e = make_float3(0.0f, 0.0f, 0.0f);
	BoxSplit(a, d, e);
	BoxSplit(b, d, e);
	BoxSplit(c, d, e);
	return (1.0f + 0.25f * (e.x + d.x)) * (1.0f + 0.25f * (e.y + d.y)) * (1.0f + 0.25f * (e.z + d.z));
}


// valid template types
template class RendererRBUC8x8<unsigned char>;
template class RendererRBUC8x8<unsigned short>;
template class RendererRBUC8x8<uchar4>;
template class RendererRBUC8x8<ushort4>;
// wrong here?