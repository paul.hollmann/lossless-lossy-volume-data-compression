// Copyright (c) 2016 Stefan Guthe / GCC / TU-Darmstadt. All rights reserved. 
// Use of this source code is governed by the BSD 3-Clause license that can be
// found in the LICENSE file.
#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define NOMINMAX

#define USING_OPEN_MP 4 //cores 0 for no omp

#include <algorithm>
#include <vector>
#include <queue>
#include <chrono>
#include "global_defines.h"
#include "../../config.h"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#ifdef USING_OPEN_MP
#include <omp.h>
#endif

#include "volumeReader_opt.cuh"

#include "helper_cuda.h"

#define __deviceinline__ __device__ inline


typedef unsigned int  uint;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned long long int ulonglong;

#ifdef WIN32
#include <Windows.h>
#endif
#include <iostream>

#ifdef PVM_SUPPORT
#include "../v3/ddsbase.cpp"
#endif

// arithmetic compression/decompression
//#include "arithcoder.h"


// verrry basic timer for testing
class timer
{
public:
	std::chrono::steady_clock::time_point times[2];

	void start()
	{
		times[0] = std::chrono::steady_clock::now();
	}
	double stop()
	{
		times[1] = std::chrono::steady_clock::now();
		double time = (double)std::chrono::duration_cast<std::chrono::milliseconds>(times[1] - times[0]).count();
		return time;
	}
	double stop(std::string str)
	{
		double time = this->stop();
		std::cout << str << " time" << ": " << time << "ms";
		return time;
	}
};

std::string print_OMP_cores()
{
	std::string result = "1core";
#ifdef USING_OPEN_MP
	#pragma omp parallel
	{
		result = std::to_string(omp_get_num_threads()) + "cores";
	}
#endif
	return result;
}


// rewrite a heck of classes and functions for the types living on the cpu or gpu ...
#include "volumeReader_opt_types.cuh"
#include "volumeReader_cpu.h"
#include "volumeReader_gpu.cuh"



template <class T, class A, class D, class O>
void quantizeVolume_full(T *vol, cudaExtent &volumeSize, int lossy, bool bruteForce, bool bruteForceOnGPU, bool relax_on_gpu)
{
#if USING_OPEN_MP
	omp_set_num_threads(USING_OPEN_MP);
#endif

	// timer
	timer t_all, t;

	t_all.start();

	t.start();
	std::vector<VolumeBlock_hd<A, O>>(blocks);
	createBricks_h<T, A, D, O>(vol, volumeSize, blocks);
	t.stop("createBricks");
	std::cout << std::endl;
	unsigned int unique_blocks = (unsigned int) blocks.size();
	unsigned int k = (unique_blocks + ((1 << lossy) - 1)) >> lossy; // more lossy = lesser number of clusters
	std::cout << unique_blocks << " unique blocks, quantizing to " << k << " blocks." << std::endl;

	std::vector<CenterBlock_hd<A, D, O>> selected_blocks(k);
	// for very large data sets & very conservative data reduction, simply update the closest cluster center for each block in parallel
	t.start();
	if (bruteForceOnGPU)
		bruteForceInit_on_gpu<T, A, D, O>(selected_blocks, blocks, k);
	else if (bruteForce) // algorithm 2 without priority queue
		bruteForceInit_h<T, A, D, O>(selected_blocks, blocks, k);
	else // algorithm 2 with priority queue
		prioQueueInit_h<T, A, D, O>(selected_blocks, blocks, k);
	t.stop(" init");
	std::cout << std::endl;

	t.start();
	// relax
	if(!relax_on_gpu)
		doRelaxation_h<T, A, D, O>(vol, blocks, selected_blocks, volumeSize, k, bruteForce);
	else
		doRelaxation_on_gpu<T, A, D, O>(vol, blocks, selected_blocks, volumeSize, k, bruteForce);
	t.stop("doRelaxation");

	std::cout << std::endl;
	t.start();
	float psnr = recreateLookuptable_h<T, A, D, O>(vol, blocks, selected_blocks, volumeSize);

	float a = quantizeMaximum_hd(vol);
	float b = 0.0f;
	psnr /= (float)(volumeSize.width * volumeSize.height * volumeSize.depth);
	psnr = (a - b) * (a - b) / psnr;
	psnr = 10.0f * logf(psnr) / logf(10.0f);
	std::cout << "PSNR: " << psnr << "db" << std::endl;
	t.stop("lookup and psnr");
	std::cout << std::endl;
	//timer
	double time = t_all.stop();
	std::cout << "->TOTAL VQ TIME: " << time << "ms = " << time / 1000.0 << "s = " << time / 60000.0 << "min" << std::endl;
}

template void quantizeVolume_full<unsigned char, unsigned long long, uint, uchar>(unsigned char *, cudaExtent &, int, bool, bool, bool);
template void quantizeVolume_full<uchar4, unsigned long long, uint4, uchar4>(uchar4 *, cudaExtent &, int , bool, bool, bool);
template void quantizeVolume_full<unsigned short, unsigned long long, unsigned long long, unsigned short>(unsigned short *, cudaExtent &, int, bool, bool, bool);
template void quantizeVolume_full<ushort4, unsigned long long, ulonglong4, ushort4>(ushort4 *, cudaExtent &, int, bool, bool, bool);

