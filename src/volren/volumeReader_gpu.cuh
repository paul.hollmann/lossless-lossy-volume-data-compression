#pragma once

// num divided by denom but rounded up
__host__ inline uint div_up(uint num, uint denom)
{
	uint res = num / denom;
	if (num % denom) ++res;
	return res;
}

// 
template<class O> __device__ inline void print_data(O);
template<> __device__ void inline print_data(uchar o) { printf("%d", o); }
template<> __device__ void inline print_data(ushort o) { printf("%d", o); }
template<> __device__ void inline print_data(uchar4 o) { printf("%d %d %d %d", o.x, o.y, o.z, o.w); }
template<> __device__ void inline print_data(ushort4 o) { printf("%d %d %d %d", o.x, o.y, o.z, o.w); }

// dot product of two block elements
template<class A, class O> __device__ inline A dot(const O& a, const O& b);
template<> __device__ inline unsigned long long dot<unsigned long long, uchar>(const uchar& a, const uchar& b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
template<> __device__ inline unsigned long long dot<unsigned long long, unsigned short>(const unsigned short& a, const unsigned short& b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
template<> __device__ inline unsigned long long dot<unsigned long long, uchar4>(const uchar4& a, const uchar4& b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }
template<> __device__ inline unsigned long long dot<unsigned long long, ushort4>(const ushort4& a, const ushort4& b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }

// add block element to a sum
template<class D, class O> __device__ inline void add(D& s, const O& v, const uint c)
{
	s += (D)(v * c);
}
template<> __device__ inline void add(uint4& s, const uchar4& v, const uint c)
{
	add<uint, uchar>(s.x, v.x, c);
	add<uint, uchar>(s.y, v.y, c);
	add<uint, uchar>(s.z, v.z, c);
	add<uint, uchar>(s.w, v.w, c);
}
template<> __device__ inline void add(ulonglong4& s, const ushort4& v, const uint c)
{
	add<ulonglong, ushort>(s.x, v.x, c);
	add<ulonglong, ushort>(s.y, v.y, c);
	add<ulonglong, ushort>(s.z, v.z, c);
	add<ulonglong, ushort>(s.w, v.w, c);
}

// convert the sum into the cc data
template<class D, class O> __device__ inline void convert(O& value, const D& sum, const uint weight, bool& changed)
{
	O temp = (O)((sum + (weight >> 1)) / weight);
	changed = changed || (temp != value);
	value = temp;
}
template<> __device__ inline void convert(uchar4& value, const uint4& sum, const uint weight, bool& changed)
{
	uchar4 temp = make_uchar4(((sum.x + (weight >> 1)) / weight), ((sum.y + (weight >> 1)) / weight), ((sum.z + (weight >> 1)) / weight), ((sum.w + (weight >> 1)) / weight));
	changed = changed || !((temp.x == value.x) && (temp.y == value.y) && (temp.z == value.z) && (temp.w == value.w));
	value = temp;
}
template<> __device__ inline void convert(ushort4& value, const ulonglong4& sum, const uint weight, bool& changed)
{
	ushort4 t = make_ushort4(
		(ushort)((sum.x + (weight >> 1)) / weight),
		(ushort)((sum.y + (weight >> 1)) / weight),
		(ushort)((sum.z + (weight >> 1)) / weight),
		(ushort)((sum.w + (weight >> 1)) / weight));
	changed = changed || !((t.x == value.x) && (t.y == value.y) && (t.z == value.z) && (t.w == value.w));
	value = t;
}

// dot product from the abs difference added to res
template<class A, class O> __device__ inline void diffdot(A& res, const O& a, const O& b)
{
	if (a < b) res += (A)((b - a) * (b - a));
	else res += (A)((a - b) * (a - b));
}
template<> __device__ inline void diffdot(unsigned long long& res, const uchar4& a, const uchar4& b)
{
	diffdot<unsigned long long, uchar>(res, a.x, b.x);
	diffdot<unsigned long long, uchar>(res, a.y, b.y);
	diffdot<unsigned long long, uchar>(res, a.z, b.z);
	diffdot<unsigned long long, uchar>(res, a.w, b.w);
}
template<> __device__ inline void diffdot(unsigned long long& res, const ushort4& a, const ushort4& b)
{
	diffdot<unsigned long long, unsigned short>(res, a.x, b.x);
	diffdot<unsigned long long, unsigned short>(res, a.y, b.y);
	diffdot<unsigned long long, unsigned short>(res, a.z, b.z);
	diffdot<unsigned long long, unsigned short>(res, a.w, b.w);
}

// distance^2 of two values
template<class A, class O>
__device__ inline A dist2(O* a, O* b, A zero_a, A zero_b)
{
	A dd = A(0);
	for (unsigned int i = 0; i < 64; i++)
	{
		dd += dot<A, O>(a[i], b[i]);
	}
	A d = zero_a + zero_b;
	d -= dd;
	d -= dd;
	return d;
}

// distance of two values
template<class A, class O>
__device__ inline double dist(O* a, O* b)
{
	A dd = dist2<A, O>(a, b);
	return sqrt((double)dd);
}


// distance^2 from origin 
template<class A, class O>
__device__ inline A dist2(O* a)
{
	A dd = A(0);
	for (unsigned int i = 0; i < 64; i++)
	{
		dd += dot<A, O>(a[i], a[i]);
	}
	return dd;
}


// KERNELS for relaxation part

template<class T, class A, class D, class O>
__global__ void prepare_cw_kernel(const uint cw_size, O* cw_data, O* cc_data, A* cw_last_dist, A* cw_zero_dist)
{
	const uint idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= cw_size) return;
	O* my_cw_data = &cw_data[64u * idx];
	A* my_cw_zero_dist = &cw_zero_dist[idx];
	*my_cw_zero_dist = dist2<A, O>(my_cw_data);
	ulonglong ulonglong_max = 0; ulonglong_max -= 1;
	cw_last_dist[idx] = ulonglong_max;
}

template<class T, class A, class D, class O>
__global__ void prepare_cc_kernel(const uint cc_size, O* cc_data, A* cc_zero_dist, bool* cc_member_changed, bool set_all_changed)
{
	const uint idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= cc_size) return;
	O* my_cc_data = &cc_data[64u * idx];
	A* my_cc_zero_dist = &cc_zero_dist[idx];
	*my_cc_zero_dist = dist2<A, O>(my_cc_data);
	cc_member_changed[idx] = set_all_changed;
}

template<class T, class A, class D, class O>
__global__ void assign_cw_kernel(const uint cw_size, const uint cc_size, O* cw_data, O* cc_data, bool* cc_changed, bool* cc_member_changed, uint* cw_cc, A* cw_last_dist2, A* cw_zero_dist2, A* cc_zero_dist2)
{
	const uint idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= cw_size) return;

	O* my_cw_data = &cw_data[64u * idx];
	A my_cw_zero_dist = cw_zero_dist2[idx];

	uint my_center = cw_cc[idx];
	uint my_old_center = my_center;
	O* my_center_data = &cc_data[my_center];
	A my_center_zero_dist = cc_zero_dist2[my_center];

	//A my_last_iteration_dist = cw_last_dist2[idx]; //unused for now
	A my_current_dist = dist2<A, O>(my_cw_data, my_center_data, my_cw_zero_dist, my_center_zero_dist);//
	if (my_current_dist == 0) return;


	for (uint i = 0; i < cc_size; i++)
	{
		A curr_cc_zero_dist = cc_zero_dist2[i];
		O* curr_cc_data = &cc_data[64u * i];

		// paper algorithm 1

		// trick does not work
		// A res = A(0);
		//diffdot<A, A>(res, my_cw_zero_dist, curr_cc_zero_dist);
		//if(res > my_current_dist)//continue;

		A curr_dist = dist2<A, O>(my_cw_data, curr_cc_data, my_cw_zero_dist, curr_cc_zero_dist);

		if (curr_dist < my_current_dist)
		{
			my_current_dist = curr_dist;
			my_center = i;
		}
		//if (curr_dist == 0) break;
	}
	if (my_center != my_old_center)
	{
		cc_member_changed[my_old_center] = true;
		cc_member_changed[my_center] = true;
		//printf("    cw %3d: cc %3d -> %3d  dist_reduction %llu \n", idx, cw_cc[idx], my_center, my_last_iteration_dist - my_current_dist);
		cw_cc[idx] = my_center;
		cw_last_dist2[idx] = my_current_dist;
	}
}

template <class T, class A, class D, class O>
__global__ void recompute_cc_kernel(const uint cw_size, const uint cc_size, O* cw_data, O* cc_data, uint* cw_weight, bool* cc_changed, bool* cc_member_changed, uint cc_changed_num, uint* cc_changed_idx, uint* cw_cc)
{
	const uint num = threadIdx.y + blockIdx.x * blockDim.y; // CAVE
	const uint warpidx = threadIdx.x;
	if (num >= cc_changed_num) return;
	//printf("tidy %d bidx %d bdimx %d bdimy %d (= %d) #%d\n", threadIdx.y, blockIdx.x, blockDim.x, blockDim.y, num, warpidx);
	const uint idx = cc_changed_idx[num];

	cc_member_changed[idx] = false; // reset the member changed flag

	O* my_cc_data = &cc_data[64u * idx];
	//D my_sum[64]; //= { D() };
	//my_sum[tidx] = D();
	//my_sum[tidx * 2] = D();
	D my_sum[2] = { D() };
	uint my_cw_count = 0;
	bool have_i_changed = false;

	for (uint i = 0; i < cw_size; i++)
	{
		if (cw_cc[i] == idx)
		{
			uint curr_cw_weight = cw_weight[i];
			O* curr_cw_data = &cw_data[64u * i];

			//for (uint ii = 0; ii < 64u; ii++) add<D, O>(my_sum[ii], curr_cw_data[ii], curr_cw_weight);
			add<D, O>(my_sum[0], curr_cw_data[warpidx], curr_cw_weight);
			add<D, O>(my_sum[1], curr_cw_data[warpidx * 2], curr_cw_weight);
			my_cw_count += curr_cw_weight;
		}
	}

	//for (uint ii = 0; ii < 64u; ii++) convert<D, O>(my_cc_data[ii], my_sum[ii], my_cw_count, have_i_changed);

	convert<D, O>(my_cc_data[warpidx], my_sum[0], my_cw_count, have_i_changed);
	convert<D, O>(my_cc_data[warpidx * 2], my_sum[1], my_cw_count, have_i_changed);

	if (have_i_changed) cc_changed[idx] = true;
}


// doRelaxation host function for the outside world ...

template <class T, class A, class D, class O>
__host__ void doRelaxation_on_gpu(T* vol, std::vector<VolumeBlock_hd<A, O>>& blocks, std::vector<CenterBlock_hd<A, D, O>>& selected_blocks, cudaExtent& volumeSize, unsigned int k, bool bruteForce) {
	std::cout << "doing relaxation on gpu: " << std::endl;
	const uint cw_size = (uint)blocks.size(); // codeword size
	const uint cc_size = (uint)selected_blocks.size(); // cluster center size

	O* cw_data_h = new O[64u * cw_size];
	O* cc_data_h = new O[64u * cc_size];
	uint* cw_weight_h = new uint[cw_size];
	bool* cc_changed_h;// = new bool[cc_size]; 
	bool* cc_member_changed_h;// = new bool[cc_size];
	uint* cc_changed_idx_h;// = new uint[cc_size];
	uint* cw_cc_h;// = new uint[cw_size];

	checkCudaErrors(cudaMallocHost(&cc_changed_h, cc_size * sizeof(bool)));
	checkCudaErrors(cudaMallocHost(&cc_member_changed_h, cc_size * sizeof(bool)));
	checkCudaErrors(cudaMallocHost(&cc_changed_idx_h, cc_size * sizeof(uint)));
	checkCudaErrors(cudaMallocHost(&cw_cc_h, cw_size * sizeof(uint)));


	{ // copy to raw cpu array
		uint i = 0;
		for (auto& cc : selected_blocks)
		{
			memcpy(&cc_data_h[64u * i], cc.value, 64u * sizeof(O));
			i++;
		}

		i = 0;

		for (auto& cw : blocks)
		{
			memcpy(&cw_data_h[64u * i], cw.value, 64u * sizeof(O));
			cw_cc_h[i] = cw.last;
			cw_weight_h[i] = cw.count;
			i++;
		}
	}

	O* cw_data_d;
	O* cc_data_d;
	uint* cw_weight_d;
	bool* cc_changed_d;
	bool* cc_member_changed_d;
	uint* cc_changed_idx_d;
	uint* cw_cc_d;
	A* cw_last_dist_d;

	A* cw_zero_dist_d;
	A* cc_zero_dist_d;

	{
		checkCudaErrors(cudaMalloc(&cw_data_d, 64u * cw_size * sizeof(O)));
		checkCudaErrors(cudaMalloc(&cc_data_d, 64u * cc_size * sizeof(O)));
		checkCudaErrors(cudaMalloc(&cw_weight_d, cw_size * sizeof(uint)));
		checkCudaErrors(cudaMalloc(&cc_changed_d, cc_size * sizeof(bool)));
		checkCudaErrors(cudaMalloc(&cc_member_changed_d, cc_size * sizeof(bool)));
		checkCudaErrors(cudaMalloc(&cc_changed_idx_d, cc_size * sizeof(uint)));
		checkCudaErrors(cudaMalloc(&cw_cc_d, cw_size * sizeof(uint)));
		checkCudaErrors(cudaMalloc(&cw_last_dist_d, cw_size * sizeof(A)));
		checkCudaErrors(cudaMalloc(&cw_zero_dist_d, cw_size * sizeof(A)));
		checkCudaErrors(cudaMalloc(&cc_zero_dist_d, cc_size * sizeof(A)));

		//cuda memcpy
		checkCudaErrors(cudaMemcpy(cw_data_d, cw_data_h, 64u * cw_size * sizeof(O), cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(cc_data_d, cc_data_h, 64u * cc_size * sizeof(O), cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(cw_weight_d, cw_weight_h, cw_size * sizeof(uint), cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(cc_changed_d, cc_changed_h, cc_size * sizeof(bool), cudaMemcpyHostToDevice));
		//checkCudaErrors(cudaMemcpy(cc_member_changed_d, cc_member_changed_h, cc_size * sizeof(bool), cudaMemcpyHostToDevice));
		checkCudaErrors(cudaMemcpy(cw_cc_d, cw_cc_h, cw_size * sizeof(uint), cudaMemcpyHostToDevice));
	}

	//now we can do the actual work
	timer t;
	//occupancy
	int minGridSize[4];
	int optBlockSize[4];
	checkCudaErrors(cudaOccupancyMaxPotentialBlockSize(&minGridSize[0], &optBlockSize[0], prepare_cw_kernel<T, A, D, O>));
	//std::cout << "prepare_cw_kernel: " << "optBlockSize" << optBlockSize << "minGridSize" << minGridSize << std::endl;
	checkCudaErrors(cudaOccupancyMaxPotentialBlockSize(&minGridSize[1], &optBlockSize[1], prepare_cc_kernel<T, A, D, O>));
	//std::cout << "prepare_cc_kernel: " << "optBlockSize" << optBlockSize << "minGridSize" << minGridSize << std::endl;
	checkCudaErrors(cudaOccupancyMaxPotentialBlockSize(&minGridSize[2], &optBlockSize[2], assign_cw_kernel<T, A, D, O>));
	//std::cout << "assign_cw_kernel: " << "optBlockSize" << optBlockSize << "minGridSize" << minGridSize << std::endl;
	checkCudaErrors(cudaOccupancyMaxPotentialBlockSize(&minGridSize[3], &optBlockSize[3], recompute_cc_kernel<T, A, D, O>));
	//std::cout << "recompute_cc_kernel: " << "optBlockSize" << optBlockSize << "minGridSize" << minGridSize << std::endl;
	uint prepare_cw_kernel_block_size = optBlockSize[0];
	uint prepare_cc_kernel_block_size = optBlockSize[1];
	uint assign_cw_kernel_block_size = optBlockSize[2];
	uint recompute_cc_kernel_block_size = optBlockSize[3] / 32; // * 32 (warp size)


	// prepare cw: compute zero dist
	dim3 prepare_cw_kernel_grid_dim(div_up(cw_size, prepare_cw_kernel_block_size), 1, 1);
	dim3 prepare_cw_kernel_block_dim(prepare_cw_kernel_block_size, 1, 1);
	prepare_cw_kernel <T, A, D, O> << <prepare_cw_kernel_grid_dim, prepare_cw_kernel_block_dim >> > (cw_size, cw_data_d, cc_data_d, cw_last_dist_d, cw_zero_dist_d);


	uint run = 1u;
	while (true)
	{
		t.start();
		if (run > 1000)
		{
			std::cout << "MAX ITERATIONS REACHED!" << std::endl;
			break;
		}
		std::cout << " gpu iteration " << run << ": "; // << std::endl;
		timer pt;
		pt.start();
		// prepare cc: recompute zero dist, reset changed members
		dim3 cc_init_grid_dim(div_up(cc_size, prepare_cc_kernel_block_size), 1, 1);
		dim3 cc_init_block_dim(prepare_cc_kernel_block_size, 1, 1);
		prepare_cc_kernel <T, A, D, O> << <cc_init_grid_dim, cc_init_block_dim >> > (cc_size, cc_data_d, cc_zero_dist_d, cc_member_changed_d, run == 1 && bruteForce);
		checkCudaErrors(cudaMemcpy(cc_member_changed_h, cc_member_changed_d, cc_size * sizeof(bool), cudaMemcpyDeviceToHost));
		pt.stop(" cc_init_kernel");
		pt.start();
		//cw kernel: map all cw to the closest cc
		dim3 cw_grid_dim(div_up(cw_size, assign_cw_kernel_block_size), 1, 1);
		dim3 cw_block_dim(assign_cw_kernel_block_size, 1, 1);
		assign_cw_kernel <T, A, D, O> << <cw_grid_dim, cw_block_dim >> > (cw_size, cc_size, cw_data_d, cc_data_d, cc_changed_d, cc_member_changed_d, cw_cc_d, cw_last_dist_d, cw_zero_dist_d, cc_zero_dist_d);
		checkCudaErrors(cudaMemcpy(cc_member_changed_h, cc_member_changed_d, cc_size * sizeof(bool), cudaMemcpyDeviceToHost));
		pt.stop(" cw_kernel");
		// check which cc had changes in their members
		uint num_cc_member_changed = 0;
		{
			for (uint i = 0; i < cc_size; i++)
			{
				if (cc_member_changed_h[i] == true)
				{
					cc_changed_idx_h[num_cc_member_changed] = i;
					num_cc_member_changed++;
				}
			}
			std::cout << "   cw in " << num_cc_member_changed << " cc changed. ";// << std::endl;
			if (num_cc_member_changed == 0)
			{
				std::cout << " Stop.";
				t.stop(" Partial iteration");
				break;
			}
		}

		checkCudaErrors(cudaMemcpy(cc_changed_idx_d, cc_changed_idx_h, cc_size * sizeof(uint), cudaMemcpyHostToDevice));
		pt.start();
		//cc kernel: recompute centers of cc with changed members
		dim3 cc_grid_dim(div_up(num_cc_member_changed, recompute_cc_kernel_block_size), 1, 1);
		dim3 cc_block_dim(32, recompute_cc_kernel_block_size, 1);
		recompute_cc_kernel<T, A, D, O> << <cc_grid_dim, cc_block_dim >> > (cw_size, cc_size, cw_data_d, cc_data_d, cw_weight_d, cc_changed_d, cc_member_changed_d, num_cc_member_changed, cc_changed_idx_d, cw_cc_d);
		checkCudaErrors(cudaMemcpy(cc_changed_h, cc_changed_d, cc_size * sizeof(bool), cudaMemcpyDeviceToHost));
		pt.stop(" cc_kernel");

		// check which cc actually moved
		{
			uint num_cc_changed = 0;
			for (uint i = 0; i < cc_size; i++)
			{
				if (cc_changed_h[i] == true) num_cc_changed++;
				cc_changed_h[i] = false;
			}
			std::cout << " " << num_cc_changed << " cc moved.";
			if (num_cc_changed == 0)
			{
				std::cout << " Stop.";
				t.stop(" Partial iteration");
				break;
			}
		}

		checkCudaErrors(cudaMemcpy(cc_changed_d, cc_changed_h, cc_size * sizeof(bool), cudaMemcpyHostToDevice));

		t.stop(" Total iteration"); std::cout << std::endl;
		//exit(0);
		run++;
	}
	std::cout << std::endl;

	//cuda memcpy
	checkCudaErrors(cudaMemcpy(cw_cc_h, cw_cc_d, cw_size * sizeof(uint), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(cc_data_h, cc_data_d, 64u * cc_size * sizeof(O), cudaMemcpyDeviceToHost));

	//put data back in vectors
	{ // copy to raw cpu array
		uint i = 0;
		for (auto& cc : selected_blocks)
		{
			for (uint ii = 0; ii < 64u; ii++)
			{
				cc.value[ii] = cc_data_h[64u * i + ii];
			}
			i++;
		}

		i = 0;
		for (auto& cw : blocks)
		{
			cw.last = cw_cc_h[i];
			i++;
		}
	}


	checkCudaErrors(cudaFree(cw_data_d));
	checkCudaErrors(cudaFree(cc_data_d));
	checkCudaErrors(cudaFree(cw_weight_d));
	checkCudaErrors(cudaFree(cc_changed_d));
	checkCudaErrors(cudaFree(cc_member_changed_d));
	checkCudaErrors(cudaFree(cc_changed_idx_d));
	checkCudaErrors(cudaFree(cw_cc_d));
	checkCudaErrors(cudaFree(cw_last_dist_d));
	checkCudaErrors(cudaFree(cw_zero_dist_d));
	checkCudaErrors(cudaFree(cc_zero_dist_d));

	delete[] cw_data_h;
	delete[] cc_data_h;
	delete[] cw_weight_h;
	checkCudaErrors(cudaFreeHost(cc_changed_h));
	checkCudaErrors(cudaFreeHost(cc_member_changed_h));
	checkCudaErrors(cudaFreeHost(cc_changed_idx_h));
	checkCudaErrors(cudaFreeHost(cw_cc_h));
}


// KERNELS for brute force init part
template<class T, class A, class D, class O>
__global__ void calculate_zero_dist(const uint cw_size, O* cw_data, A* cw_zero_dist)
{
	const uint idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= cw_size) return;
	O* my_cw_data = &cw_data[64u * idx];
	cw_zero_dist[idx] = dist2<A, O>(my_cw_data);
}

template<class T, class A, class D, class O>
__global__ void calculate_dist(const uint cw_size, O* cw_data, uint cw_center_idx, A* cw_zero_dist, A* cw_dist)
{
	const uint idx = threadIdx.x + blockIdx.x * blockDim.x;
	if (idx >= cw_size) return;
	O* my_cw_data = &cw_data[64u * idx];
	O* other_cw_data = &cw_data[64u * cw_center_idx];
	A my_cw_zero_dist = cw_zero_dist[idx];
	A other_cw_zero_dist = cw_zero_dist[cw_center_idx];

	cw_dist[idx] = dist2<A, O>(my_cw_data, other_cw_data, my_cw_zero_dist, other_cw_zero_dist);
}

// bruteForceInit host function for the outside world ...
template <class T, class A, class D, class O>
__host__ void bruteForceInit_on_gpu(std::vector<CenterBlock_hd<A, D, O>>& selected_blocks, std::vector<VolumeBlock_hd<A, O>>& blocks, unsigned int k)
{
	std::cout << "brute force init on gpu:" << std::endl;
	const uint cw_size = (uint)blocks.size(); // codeword size
	const uint cc_size = (uint)selected_blocks.size(); // cluster center size
	k = cc_size;

	O* cw_data_h = new O[64ull * cw_size];
	A* cw_dist_array_h = new A[cw_size];

	O* cw_data_d;
	A* cw_zero_dist_d;
	A* cw_dist_array_d;
	checkCudaErrors(cudaMalloc(&cw_data_d, 64ull * cw_size * sizeof(O)));
	checkCudaErrors(cudaMalloc(&cw_zero_dist_d, cw_size * sizeof(A)));
	checkCudaErrors(cudaMalloc(&cw_dist_array_d, cw_size * sizeof(A)));

	
	{ // copy to raw cpu array
		uint i = 0;
		for (auto& cw : blocks)
		{
			memcpy(&cw_data_h[64ull * i], cw.value, 64u * sizeof(O));
			i++;
		}
	}

	checkCudaErrors(cudaMemcpy(cw_data_d, cw_data_h, 64ull * cw_size * sizeof(O), cudaMemcpyHostToDevice));

	constexpr uint cw_kernel_block_dim = 265u;
	//now we can start


	// STEP 1

	A global_min = A(0x7fffffffffffffffll);
	uint min_idx = 0;
	// compute zero dist on GPU
	{
		dim3 cw_init_grid_dim(div_up(cw_size, cw_kernel_block_dim), 1, 1);
		dim3 cw_init_block_dim(cw_kernel_block_dim, 1, 1);
		calculate_zero_dist<T, A, D, O> << <cw_init_grid_dim, cw_init_block_dim >> > (cw_size, cw_data_d, cw_zero_dist_d);
		checkCudaErrors(cudaMemcpy(cw_dist_array_h, cw_zero_dist_d, cw_size * sizeof(A), cudaMemcpyDeviceToHost)); //! cw_zero_dist_d --> cw_dist_array_h
	}
	// #pragma omp parallel for reduction()
	for (uint i = 0; i < cw_size; i++)
	{
		A p = cw_dist_array_h[i];
		//p = blocks[i].dist();// * blocks[i].count;
		if ((p < global_min) || ((p == global_min) && (i < min_idx)))
		{
			global_min = p;
			min_idx = i;
		}
	}
	selected_blocks[0] = blocks[min_idx];
	
	// STEP 2

	uint next_idx = 0;
	A global_max = A(0);
	//CenterBlock_hd<A, D, O>& center = selected_blocks[0];
	// compute the dist to this center on GPU
	{
		uint cw_center_idx = min_idx;
		dim3 cw_init_grid_dim(div_up(cw_size, cw_kernel_block_dim), 1, 1);
		dim3 cw_init_block_dim(cw_kernel_block_dim, 1, 1);
		calculate_dist<T, A, D, O> << <cw_init_grid_dim, cw_init_block_dim >> > (cw_size, cw_data_d, cw_center_idx, cw_zero_dist_d, cw_dist_array_d);
		checkCudaErrors(cudaMemcpy(cw_dist_array_h, cw_dist_array_d, cw_size * sizeof(A), cudaMemcpyDeviceToHost));
	}
	for (uint i = 0; i < cw_size; i++)
	{
		VolumeBlock_hd<A, O>& blk = blocks[i];
		A p = blk.lastDist = cw_dist_array_h[i];//center.dist(blk);
		blk.last = 0;
		if ((p > global_max) || ((p == global_max) && (i < next_idx)))
		{
			global_max = p;
			next_idx = i;
		}
	}

	// STEP 3 still needs CUDA :)

	unsigned int done = 1;
	while (done < k)
	{
		selected_blocks[done] = blocks[next_idx];
		// update distances
		global_max = A(0);

		// compute the dist to this center on GPU
		{
			uint cw_center_idx = next_idx;
			dim3 cw_init_grid_dim(div_up(cw_size, cw_kernel_block_dim), 1, 1);
			dim3 cw_init_block_dim(cw_kernel_block_dim, 1, 1);
			calculate_dist<T, A, D, O> << <cw_init_grid_dim, cw_init_block_dim >> > (cw_size, cw_data_d, cw_center_idx, cw_zero_dist_d, cw_dist_array_d);
			checkCudaErrors(cudaMemcpy(cw_dist_array_h, cw_dist_array_d, cw_size * sizeof(A), cudaMemcpyDeviceToHost));
		}
		next_idx = 0;
		//CenterBlock_hd<A, D, O>& center = selected_blocks[done];

		for (uint i = 0; i < cw_size; i++)
		{
			VolumeBlock_hd<A, O>& blk = blocks[i];
			A current_dist = cw_dist_array_h[i];

			//if (center.dist(blk, blk.lastDist, false))
			if(current_dist < blk.lastDist)
			{
				blk.lastDist = current_dist; //!
				blk.last = done;
			}
			A p = blk.lastDist;
			if ((done + 1 < k) && (p >= global_max))
			{
				if ((p > global_max) || ((p == global_max) && (i < next_idx)))
				{
					global_max = p;
					next_idx = i;
				}
			}
		}
		done++;
		if ((done * 80 / k) > ((done - 1) * 80 / k)) std::cout << ".";
	}
	


	// DONE

	delete[] cw_data_h;
	delete[] cw_dist_array_h;
	checkCudaErrors(cudaFree(cw_data_d));
	checkCudaErrors(cudaFree(cw_zero_dist_d));
	checkCudaErrors(cudaFree(cw_dist_array_d));
}