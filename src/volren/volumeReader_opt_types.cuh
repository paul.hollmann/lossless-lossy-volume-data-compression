#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define NOMINMAX


typedef unsigned int  uint;
typedef unsigned char uchar;

template <class T>  T zero_hd();

template<> uchar zero_hd() { return 0; }
template<> unsigned short zero_hd() { return 0; }
template<> uint zero_hd() { return 0; }
template<> unsigned long long zero_hd() { return 0; }
template<> uchar4 zero_hd() { return make_uchar4(0, 0, 0, 0); }
template<> ushort4 zero_hd() { return make_ushort4(0, 0, 0, 0); }
template<> uint4 zero_hd() { return make_uint4(0, 0, 0, 0); }
template<> ulonglong4 zero_hd() { return make_ulonglong4(0, 0, 0, 0); }

// one brick of 4x4x4=64 voxels
template<class A, class D>
class VolumeBlock_hd
{
public:
	D value[64];
	unsigned short hash;
	unsigned int count;
	unsigned int last;

	A lastDist;
	A squared;
	double zeroDist;

	__forceinline A dot(const D& a, const D& b);
	__forceinline bool equal(const D& a, const D& b);
	__forceinline void hashing(const D& a);

	__forceinline bool operator==(VolumeBlock_hd<A, D>& a)
	{
		if (a.hash != hash) return false;
		for (unsigned int i = 0; i < 64; i++) if (!equal(value[i], a.value[i])) return false;
		return true;
	}

	__forceinline void calcHash()
	{
		hash = 0;
		for (unsigned int i = 0; i < 64; i++) hashing(value[i]);
		squared = A(0);
		for (unsigned int i = 0; i < 64; i++) squared += dot(value[i], value[i]);
		zeroDist = sqrt((double)squared);
	}

	__forceinline A dist(const VolumeBlock_hd<A, D>& v)
	{
		A dd = A(0);
		for (unsigned int i = 0; i < 64; i++)
		{
			dd += dot(value[i], v.value[i]);
		}
		A d = squared + v.squared;
		d -= dd;
		d -= dd;
		return d;
	}

	__forceinline A dist()
	{
		return squared;
	}

	__forceinline bool dist(VolumeBlock_hd<A, D>& v, A min_dist, A& d) // algorithm 1
	{
		if ((zeroDist - v.zeroDist) * (zeroDist - v.zeroDist) > min_dist) return false;
		A dd = A(0);
		for (unsigned int i = 0; i < 64; i++)
		{
			dd += dot(value[i], v.value[i]);
		}
		d = squared + v.squared;
		d -= dd;
		d -= dd;
		if (d < min_dist)
		{
			return true;
		}
		return false;
	}
};

template<> __forceinline unsigned long long VolumeBlock_hd<unsigned long long, uchar>::dot(const uchar& a, const uchar& b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
template<> __forceinline unsigned long long VolumeBlock_hd<unsigned long long, unsigned short>::dot(const unsigned short& a, const unsigned short& b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
template<> __forceinline unsigned long long VolumeBlock_hd<unsigned long long, uchar4>::dot(const uchar4& a, const uchar4& b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }
template<> __forceinline unsigned long long VolumeBlock_hd<unsigned long long, ushort4>::dot(const ushort4& a, const ushort4& b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }

template<> __forceinline bool VolumeBlock_hd<unsigned long long, uchar>::equal(const uchar& a, const uchar& b) { return a == b; }
template<> __forceinline bool VolumeBlock_hd<unsigned long long, unsigned short>::equal(const unsigned short& a, const unsigned short& b) { return a == b; }
template<> __forceinline bool VolumeBlock_hd<unsigned long long, uchar4>::equal(const uchar4& a, const uchar4& b) { return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w); }
template<> __forceinline bool VolumeBlock_hd<unsigned long long, ushort4>::equal(const ushort4& a, const ushort4& b) { return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w); }

template<> __forceinline void VolumeBlock_hd<unsigned long long, uchar>::hashing(const uchar& a) { hash = (hash << 1) ^ (hash >> 15) ^ a; }
template<> __forceinline void VolumeBlock_hd<unsigned long long, unsigned short>::hashing(const unsigned short& a) { hash = (hash << 1) ^ (hash >> 15) ^ a; }
template<> __forceinline void VolumeBlock_hd<unsigned long long, uchar4>::hashing(const uchar4& a) { hash = (hash << 1) ^ (hash >> 15) ^ a.x; hash = (hash << 1) ^ (hash >> 15) ^ a.y; hash = (hash << 1) ^ (hash >> 15) ^ a.z; hash = (hash << 1) ^ (hash >> 15) ^ a.w; }
template<> __forceinline void VolumeBlock_hd<unsigned long long, ushort4>::hashing(const ushort4& a) { hash = (hash << 1) ^ (hash >> 15) ^ a.x; hash = (hash << 1) ^ (hash >> 15) ^ a.y; hash = (hash << 1) ^ (hash >> 15) ^ a.z; hash = (hash << 1) ^ (hash >> 15) ^ a.w; }


template <class A, class D, class O>
class CenterBlock_hd
{
private:
#ifdef USING_OPEN_MP
	omp_lock_t lock;
#endif

public:
	O value[64];
	D sum[64];
	uint weight;
	bool changedCenter;
	bool changedMember;
	A squared;
	double zeroDist;

	 CenterBlock_hd() {
#ifdef USING_OPEN_MP
		omp_init_lock(&lock);
#endif
	}

	__forceinline A dot(const O& a, const O& b);
	__forceinline A dot2(const O& a, const D& b);
	__forceinline void add(D& s, const O& v, const int c);
	__forceinline void convert(O& v, const D& s);
	__forceinline void assign(O& v, const D& s);

	__forceinline A dist(VolumeBlock_hd<A, O>& v)
	{
		A dd = A(0);
		for (unsigned int i = 0; i < 64; i++)
		{
			dd += dot(value[i], v.value[i]);
		}
		A d = squared + v.squared;
		d -= dd;
		d -= dd;
		return d;
	}

	__forceinline bool dist(VolumeBlock_hd<A, O>& v, A& min_dist, bool eq) // algorithm 1
	{
		if ((zeroDist - v.zeroDist) * (zeroDist - v.zeroDist) > min_dist) return false;
		A dd = A(0);
		for (unsigned int i = 0; i < 64; i++)
		{
			dd += dot(value[i], v.value[i]);
		}
		A d = squared + v.squared;
		d -= dd;
		d -= dd;
		if ((d < min_dist) || ((d == min_dist) && eq))
		{
			min_dist = d;
			return true;
		}
		return false;
	}

	__forceinline void add(const VolumeBlock_hd<A, O>& v)
	{
#ifdef USING_OPEN_MP
		omp_set_lock(&lock);
#endif
		for (unsigned int i = 0; i < 64; i++)
		{
			add(sum[i], v.value[i], (int)v.count);
		}
		weight += v.count;
#ifdef USING_OPEN_MP
		omp_unset_lock(&lock);
#endif
	}

	__forceinline void sub(const VolumeBlock_hd<A, O>& v)
	{
#ifdef USING_OPEN_MP
		omp_set_lock(&lock);
#endif
		for (unsigned int i = 0; i < 64; i++)
		{
			add(sum[i], v.value[i], -((int)v.count));
		}
		weight -= v.count;
#ifdef USING_OPEN_MP
		omp_unset_lock(&lock);
#endif
	}

	__forceinline void update()
	{
		changedCenter = false;
		if (!changedMember) return;
		changedMember = false;
		if (weight > 0)
		{
			squared = A(0);
			for (unsigned int i = 0; i < 64; i++)
			{
				convert(value[i], sum[i]);
			}
			zeroDist = sqrt((double)squared);
		}
	}
	__forceinline CenterBlock_hd<A, D, O>& operator=(VolumeBlock_hd<A, O>& a)
	{
		for (unsigned int i = 0; i < 64; i++)
		{
			//assign(value[i], a.value[i]);
			value[i] = a.value[i];
		}
		for (unsigned int i = 0; i < 64; i++)
		{
			sum[i] = zero_hd<D>();
		}
		weight = 0;
		changedCenter = true;
		changedMember = true;
		squared = a.squared;
		zeroDist = a.zeroDist;
		return *this;
	}
};

template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, uint, uchar>::dot2(const uchar& a, const uint& b) { return (unsigned long long)a * (unsigned long long)b; }
template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>::dot2(const unsigned short& a, const unsigned long long& b) { return (unsigned long long)a * b; }
template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, uint4, uchar4>::dot2(const uchar4& a, const uint4& b) { return (unsigned long long)a.x * (unsigned long long)b.x + (unsigned long long)a.y * (unsigned long long)b.y + (unsigned long long)a.z * (unsigned long long)b.z + (unsigned long long)a.w * (unsigned long long)b.w; }
template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, ulonglong4, ushort4>::dot2(const ushort4& a, const ulonglong4& b) { return (unsigned long long)a.x * b.x + (unsigned long long)a.y * b.y + (unsigned long long)a.z * b.z + (unsigned long long)a.w * b.w; }

template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, uint, uchar>::dot(const uchar& a, const uchar& b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>::dot(const unsigned short& a, const unsigned short& b) { return (unsigned long long)((unsigned int)a * (unsigned int)b); }
template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, uint4, uchar4>::dot(const uchar4& a, const uchar4& b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }
template<> __forceinline unsigned long long CenterBlock_hd<unsigned long long, ulonglong4, ushort4>::dot(const ushort4& a, const ushort4& b) { return (unsigned long long)((unsigned int)a.x * (unsigned int)b.x + (unsigned int)a.y * (unsigned int)b.y + (unsigned int)a.z * (unsigned int)b.z + (unsigned int)a.w * (unsigned int)b.w); }

template<> __forceinline void CenterBlock_hd<unsigned long long, uint, uchar>::add(uint& s, const uchar& v, const int c)
{
	s += (int)(v * c);
}
template<> __forceinline void CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>::add(unsigned long long& s, const unsigned short& v, const int c)
{
	s += (long long)v * (long long)c;
}
template<> __forceinline void CenterBlock_hd<unsigned long long, uint4, uchar4>::add(uint4& s, const uchar4& v, const int c)
{

	s.x += (int)v.x * c;
	s.y += (int)v.y * c;
	s.z += (int)v.z * c;
	s.w += (int)v.w * c;
}
template<> __forceinline void CenterBlock_hd<unsigned long long, ulonglong4, ushort4>::add(ulonglong4& s, const ushort4& v, const int c)
{

	s.x += (long long)v.x * (long long)c;
	s.y += (long long)v.y * (long long)c;
	s.z += (long long)v.z * (long long)c;
	s.w += (long long)v.w * (long long)c;
}

template<> __forceinline void CenterBlock_hd<unsigned long long, uint, uchar>::convert(uchar& v, const uint& s) {
	uchar t = ((s + (weight >> 1)) / weight);
	changedCenter |= (t != v);
	v = t;
	squared += dot(t, t);
}
template<> __forceinline void CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>::convert(unsigned short& v, const unsigned long long& s) {
	unsigned short t = (unsigned short)((s + (weight >> 1)) / weight);
	changedCenter |= (t != v);
	v = t;
	squared += dot(t, t);
}
template<> __forceinline void CenterBlock_hd<unsigned long long, uint4, uchar4>::convert(uchar4& v, const uint4& s) {
	uchar4 t = make_uchar4(((s.x + (weight >> 1)) / weight), ((s.y + (weight >> 1)) / weight), ((s.z + (weight >> 1)) / weight), ((s.w + (weight >> 1)) / weight));
	changedCenter |= !((t.x == v.x) && (t.y == v.y) && (t.z == v.z) && (t.w == v.w));
	v = t;
	squared += dot(t, t);
}
template<> __forceinline void CenterBlock_hd<unsigned long long, ulonglong4, ushort4>::convert(ushort4& v, const ulonglong4& s) {
	ushort4 t = make_ushort4(
		(unsigned short)((s.x + (weight >> 1)) / weight),
		(unsigned short)((s.y + (weight >> 1)) / weight),
		(unsigned short)((s.z + (weight >> 1)) / weight),
		(unsigned short)((s.w + (weight >> 1)) / weight));
	changedCenter |= !((t.x == v.x) && (t.y == v.y) && (t.z == v.z) && (t.w == v.w));
	v = t;
	squared += dot(t, t);
}

template<> __forceinline void CenterBlock_hd<unsigned long long, uint, uchar>::assign(uchar& v, const uint& s) { v = s; }
template<> __forceinline void CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>::assign(unsigned short& v, const unsigned long long& s) { v = (unsigned short)s; }
template<> __forceinline void CenterBlock_hd<unsigned long long, uint4, uchar4>::assign(uchar4& v, const uint4& s) { v = make_uchar4(s.x, s.y, s.z, s.w); }
template<> __forceinline void CenterBlock_hd<unsigned long long, ulonglong4, ushort4>::assign(ushort4& v, const ulonglong4& s) { v = make_ushort4((unsigned short)s.x, (unsigned short)s.y, (unsigned short)s.z, (unsigned short)s.w); }


template <typename T>
class LessThan_hd
{
public:
	 LessThan_hd() { }
	 bool operator() (const T& lhs, const T& rhs) const
	{
		return (lhs.p < rhs.p);
	}
};

template <class A>
class IndexedPriority_hd
{
public:
	A p;
	unsigned int idx;
	unsigned int count;
	 IndexedPriority_hd(unsigned int i, unsigned int c, A pri) { idx = i; p = pri; count = c; }
	 IndexedPriority_hd() {} //why?
};

template <class VOL, class T>  float calcPsnr_hd(T& vol, VOL& sel, int v);

template <>  float calcPsnr_hd<CenterBlock_hd<unsigned long long, uint, uchar>, unsigned char>(unsigned char& vol, CenterBlock_hd<unsigned long long, uint, uchar>&sel, int v)
{
	float psnr = 0.0f;
	uchar val = sel.value[v];
	psnr = (float)(((int)vol - (int)val) * ((int)vol - (int)val));
	vol = (unsigned char)val;
	return psnr;
}
template <>  float calcPsnr_hd<CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>, unsigned short>(unsigned short& vol, CenterBlock_hd<unsigned long long, unsigned long long, unsigned short>&sel, int v)
{
	float psnr = 0.0f;
	unsigned short val = sel.value[v];
	psnr = (float)(((int)vol - (int)val) * ((int)vol - (int)val));
	vol = val;
	return psnr;
}
template <>  float calcPsnr_hd<CenterBlock_hd<unsigned long long, uint4, uchar4>, uchar4>(uchar4 & vol, CenterBlock_hd<unsigned long long, uint4, uchar4>&sel, int v)
{
	float psnr = 0.0f;
	uchar4 val = sel.value[v];
	psnr = (float)(((int)vol.x - (int)val.x) * ((int)vol.x - (int)val.x));
	psnr += (float)(((int)vol.y - (int)val.y) * ((int)vol.y - (int)val.y));
	psnr += (float)(((int)vol.z - (int)val.z) * ((int)vol.z - (int)val.z));
	psnr += (float)(((int)vol.w - (int)val.w) * ((int)vol.w - (int)val.w));
	vol = val;
	return psnr;
}
template <>  float calcPsnr_hd<CenterBlock_hd<unsigned long long, ulonglong4, ushort4>, ushort4>(ushort4 & vol, CenterBlock_hd<unsigned long long, ulonglong4, ushort4>&sel, int v)
{
	float psnr = 0.0f;
	ushort4 val = sel.value[v];
	psnr = (float)(((int)vol.x - (int)val.x) * ((int)vol.x - (int)val.x));
	psnr += (float)(((int)vol.y - (int)val.y) * ((int)vol.y - (int)val.y));
	psnr += (float)(((int)vol.z - (int)val.z) * ((int)vol.z - (int)val.z));
	psnr += (float)(((int)vol.w - (int)val.w) * ((int)vol.w - (int)val.w));
	vol = val;
	return psnr;
}


 float quantizeMaximum_hd(unsigned char* vol) { return 255.0f; }
 float quantizeMaximum_hd(unsigned short* vol) { return 4095.0f; }
 float quantizeMaximum_hd(uchar4* vol) { return 255.0f; }
 float quantizeMaximum_hd(ushort4* vol) { return 4095.0f; }

