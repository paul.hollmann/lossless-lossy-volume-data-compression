# Lossless-Lossy Volume Data Compression

## possibe steps for optimization
(in order in which they will be called and executed)

I guess we will have to (re)implement all of these on CPU and on GPU. Maybe its better to do the CPU versions first (for which we can look at the already given implmentations). Thereafter we can do step by step GPU implmentations and compare performace of each version and choose the faster one for each step.

- vector quantisation (see volumeReader_opt...)
    - hash based brick filtering
    - vector quantisation init algo
    - vector quantisation main relaxation part

- RBUC compression (see RendererRBUC8x8_opt...)
    - initial codebook cration and hash based filtering 
    - execution of the tranformations and choosing the best one

